using StardewModdingAPI.Utilities;

namespace SVNotepad{
	class ModConfig{
		public KeybindList ToggleKey { get; set; } = KeybindList.Parse( "N" ); // Key to open the mod's menu
	}
}