using System.Collections.Generic;

namespace SVNotepad{
	class ModData{

		/********************
		**	Properties
		********************/
		public List<Note> Notes { get; set; }
		
		/******************
		**	Constructors
		******************/
		public ModData(){
			this.Notes = new List<Note>();
		}
		
		public ModData( List<Note> Notes ){
			this.Notes = Notes;
		}
	}
}